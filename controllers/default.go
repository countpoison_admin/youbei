package controllers

import (
	"github.com/astaxie/beego"
)

// ResAPI ...
type ResAPI struct {
	Success int         `json:"success"`
	Msg     string      `json:"msg"`
	Result  interface{} `json:"result"`
}

// MainController ...
type MainController struct {
	beego.Controller
}

// LoginController ...
type LoginController struct {
	beego.Controller
}

// APIReturn ...
func (c *MainController) APIReturn(Success int, Msg string, data interface{}) {
	res := new(ResAPI)
	if Success != 200 && data == nil {
		return
	}
	res.Result = data
	res.Success = Success
	res.Msg = Msg
	res.Result = data
	c.Data["json"] = res
	c.Ctx.ResponseWriter.WriteHeader(Success)
	c.ServeJSON()
	c.StopRun()
}

// APIReturn ...
func (c *LoginController) APIReturn(Success int, Msg string, data interface{}) {
	res := new(ResAPI)
	if Success != 200 && data == nil {
		return
	}
	res.Result = data
	res.Success = Success
	res.Msg = Msg
	c.Data["json"] = res
	c.Ctx.ResponseWriter.WriteHeader(Success)
	c.ServeJSON()
	c.StopRun()
}
