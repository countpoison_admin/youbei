package controllers

import (
	"errors"

	md "gitee.com/countpoison/youbei/models"
)

// ShowrLog ...
func (c *MainController) ShowrLog() {
	id := c.Ctx.Input.Param(":id")
	rlog := new(md.Rlog)
	bol, err := md.Localdb().ID(id).Get(rlog)
	c.APIReturn(500, "获取远程传输日志失败", err)
	if !bol {
		c.APIReturn(500, "日志不存在", errors.New("日志不存在"))
	}
	loginfo := md.Log{}
	bol, err = md.Localdb().ID(rlog.Lid).Get(&loginfo)
	c.APIReturn(500, "获取备份日志失败", err)
	if !bol {
		c.APIReturn(500, "备份日志不存在", errors.New("日志不存在"))
	}
	rlog.LogInfo = loginfo

	dbinfo := md.Task{}
	bol, err = md.Localdb().ID(rlog.Tid).Get(&dbinfo)
	c.APIReturn(500, "获取备份信息失败", err)
	if !bol {
		c.APIReturn(500, "备份信息不存在", errors.New("日志不存在"))
	}
	rlog.DBInfo = dbinfo

	rsinfo := md.RemoteStorage{}
	bol, err = md.Localdb().ID(rlog.Rid).Get(&rsinfo)
	c.APIReturn(500, "获取远程信息失败", err)
	if !bol {
		c.APIReturn(500, "远程信息不存在", errors.New("日志不存在"))
	}
	rlog.RSInfo = rsinfo

	if rlog.RSInfo.Types == "Yserver" {
		yufinfo := md.YsUploadFile{}

		bol, err := md.Localdb().Where("lid=?", rlog.ID).Get(&yufinfo)
		c.APIReturn(500, "获取上传文件信息失败", err)
		if !bol {
			c.APIReturn(500, "上传文件信息不存在", errors.New("日志不存在"))
		}

		ypinfo := []md.YsPackets{}
		c.APIReturn(500, "获取上传文件切片信息失败", md.Localdb().Where("yid=?", yufinfo.ID).Asc("sortid").Find(&ypinfo))
		yufinfo.YsPackets = ypinfo
		rlog.YsUploadFile = yufinfo
	}

	c.APIReturn(200, "成功", rlog)
}

// ShowLog ...
func (c *MainController) ShowLog() {
	id := c.Ctx.Input.Param(":id")
	loginfo := new(md.Log)
	bol, err := md.Localdb().ID(id).Get(loginfo)
	c.APIReturn(500, "获取远备份日志失败", err)
	if !bol {
		c.APIReturn(500, "日志不存在", errors.New("日志不存在"))
	}

	dbinfo := md.Task{}
	bol, err = md.Localdb().ID(loginfo.Tid).Get(&dbinfo)
	c.APIReturn(500, "获取备份信息失败", err)
	if !bol {
		c.APIReturn(500, "备份信息不存在", errors.New("日志不存在"))
	}
	loginfo.DBInfo = dbinfo

	c.APIReturn(200, "成功", loginfo)
}

// GetYserverLog ...
func (c *MainController) GetYserverLog() {
	id := c.Ctx.Input.Param(":id")
	yfinfo := new(md.YserverFile)
	bol, err := md.Localdb().ID(id).Get(yfinfo)
	c.APIReturn(500, "获取日志失败", err)
	if !bol {
		c.APIReturn(500, "日志不存在", errors.New("日志不存在"))
	}
	ypsinfo := []md.YserverPacket{}
	c.APIReturn(500, "获取备份信息失败", md.Localdb().Where("fid=?", yfinfo.ID).Asc("sort").Find(&ypsinfo))
	yfinfo.YserverPackets = ypsinfo
	c.APIReturn(200, "成功", yfinfo)
}
