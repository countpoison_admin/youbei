package md

import (
	"os"
	"time"

	// gosqlite3
	_ "github.com/mattn/go-sqlite3"
	"github.com/xormplus/xorm"
)

var localdb *xorm.Engine

// Init 初始化数据库
func Init(dir string) {
	err := os.MkdirAll(dir, 0666)
	if err != nil {
		panic(err)
	}
	localdb, err = xorm.NewEngine("sqlite3", dir+"/local.db")
	if err != nil {
		panic(err)
	}
	err = localdb.Sync2(new(Task),
		new(Log),
		new(User),
		new(RemoteStorage),
		new(RemoteStorageToTask),
		new(Rlog), new(Yserver),
		new(YserverFile),
		new(YserverPacket),
		new(YsUploadFile),
		new(YsPackets),
		new(MailServer),
		new(Host),
		new(MailSend),
		new(SshTask))
	if err != nil {
		panic(err)
	}
	UserInit()
	InitYservefunc()
	MailServerInit()
}

// Localdb 返回数据库对象
func Localdb() *xorm.Engine {
	return localdb
}

// NowTime 返回当前时间戳
func NowTime() int64 {
	return time.Now().Unix()
}
