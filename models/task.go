package md

import (
	"errors"
	"fmt"
	"time"

	// gosqlite3
	_ "github.com/mattn/go-sqlite3"
)

// Task ...
type Task struct {
	ID       string   `json:"id" xorm:"pk notnull unique 'id'"`
	DBType   string   `json:"dbtype" xorm:" 'dbtype'"`
	Host     string   `json:"host" xorm:" 'host'"`
	Port     string   `json:"port" xorm:" 'port'"`
	User     string   `json:"user" xorm:" 'user'"`
	Password string   `json:"password" xorm:" 'password'"`
	DBname   string   `json:"dbname" xorm:" 'dbname'"`
	Char     string   `json:"char" xorm:" 'char'"`
	DBpath   string   `json:"dbpath" xorm:" 'dbpath'"`
	Created  int64    `json:"created" xorm:"notnull 'created'"`
	Pause    string   `json:"pause" xorm:" 'pause'"`
	Status   string   `json:"status" xorm:" 'status'"`
	Crontab  string   `json:"crontab" xorm:"notnull 'crontab'"`
	SavePath string   `json:"savepath" xorm:"notnull 'savepath'"`
	RS       []string `json:"rs" xorm:"-"`
	Zippwd   string   `json:"zippwd" xorm:"'zippwd'"`
	Expire   int      `json:"expire" xorm:"'expire'"`
}

// Add ...
func (t *Task) Add() error {
	t.Created = time.Now().Unix()
	_, err := localdb.Insert(t)
	if err != nil {
		return err
	}
	return nil
}

// Update ...
func (t *Task) Update() error {
	_, err := localdb.ID(t.ID).Update(t)
	if err != nil {
		return err
	}
	return nil
}

// Select ...
func (t *Task) Select() error {
	bol, err := localdb.ID(t.ID).Get(t)
	if err != nil {
		return err
	}
	if !bol {
		return errors.New("not found")
	}
	rs := []RemoteStorageToTask{}
	if err := localdb.Where("tid=?", t.ID).Find(&rs); err != nil {
		return err
	}
	if len(rs) > 0 {
		for _, s := range rs {
			t.RS = append(t.RS, s.Rid)
		}
	}
	return nil
}

// SelectAll ...
func SelectAll(typestr string, page, count int) ([]Task, error) {
	var err error
	tasks := []Task{}
	var types []string
	if typestr == "file" {
		types = []string{`file`}
	} else {
		types = []string{`sqlite3`, `mysql`, `mssql`}
	}
	if err := localdb.In("dbtype", types).Desc("created").Limit(count, count*(page-1)).Find(&tasks); err != nil {
		fmt.Println(2)
		return []Task{}, err
	}
	for k, v := range tasks {
		rs := []RemoteStorageToTask{}
		rss := []string{}
		if err := localdb.Where("tid=?", v.ID).Find(&rs); err != nil {
			fmt.Println(1)
			return []Task{}, err
		}
		if len(rs) > 0 {
			for _, s := range rs {
				rss = append(rss, s.Rid)
			}
		}
		tasks[k].RS = rss
	}
	return tasks, err
}

// TaskCount ...
func TaskCount(typestr string) (int64, error) {
	var err error
	tasks := Task{}
	var types []string
	if typestr == "file" {
		types = []string{`file`}
	} else {
		types = []string{`sqlite3`, `mysql`, `mssql`}
	}
	title, err := localdb.In("dbtype", types).Count(&tasks)
	return title, err
}

// All ...
func All() ([]Task, error) {
	t := []Task{}
	err := localdb.Where("pause=?", "no").Find(&t)
	if err != nil {
		return t, err
	}
	return t, nil
}

// Delete ...
func (t *Task) Delete() error {
	_, err := localdb.ID(t.ID).Delete(t)
	if err != nil {
		return err
	}
	err = TaskDeleteALL(t.ID)
	return err
}

// TaskDeleteALL ...
func TaskDeleteALL(tid string) error {
	if _, err := localdb.Exec("delete from log where tid = ?", tid); err != nil {
		return err
	}
	if _, err := localdb.Exec("delete from rlog where tid = ?", tid); err != nil {
		return err
	}
	if _, err := localdb.Exec("delete from remote_storage_to_task where tid = ?", tid); err != nil {
		return err
	}
	return nil
}
