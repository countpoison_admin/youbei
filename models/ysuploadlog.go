package md

import (
	"fmt"
	"time"
)

// YsUploadFile ...
type YsUploadFile struct {
	ID                 string      `json:"id" xorm:"pk notnull unique 'id'"`
	Lid                string      `json:"lid" xorm:"'lid'"`
	UploadFileServerID string      `json:"ufsid" xorm:"'ufsid'"`
	SrcFilePath        string      `json:"srcfilepath" xorm:"'srcfilepath'"`
	Created            int64       `json:"created" xorm:"'created'"`
	Size               int64       `json:"size" xorm:"'size'"`
	PacketNum          int64       `json:"packetnum" xorm:"'packetnum'"`
	Status             int         `json:"status" xorm:"'status'"`
	Msg                string      `json:"msg" xorm:"'msg'"`
	YsPackets          []YsPackets `json:"packets" xorm:"-"`
}

// YsPackets ...
type YsPackets struct {
	ID              string `json:"id" xorm:"pk notnull unique 'id'"`
	Yid             string `json:"yid" xorm:"'yid'"`
	SortID          int    `json:"sortid" xorm:"'sortid'"`
	SrcPacketPath   string `json:"srcpacketpath" xorm:"'srcpacketpath'"`
	Offset          int64  `json:"offset" xorm:"'offset'"`
	UploadPacketURL string `json:"uploadpacketurl" xorm:"'uploadpacketurl'"`
	Status          int    `json:"status" xorm:"'status'"`
	Msg             string `json:"msg" xorm:"'msg'"`
}

// AddYsFileLog ...
func (yuf *YsUploadFile) AddYsFileLog(err error) error {
	yuf.Created = time.Now().Unix()
	if err != nil {
		yuf.Status = 2
		yuf.Msg = err.Error()
	} else {
		yuf.Status = 1
	}
	_, err = localdb.Insert(yuf)
	if err != nil {
		fmt.Println(err.Error())
	}
	for _, packet := range yuf.YsPackets {
		if err := yuf.AddYsPacketLog(packet); err != nil {
			fmt.Println(err.Error())
		}
	}
	return err
}

// AddYsPacketLog ...
func (yuf *YsUploadFile) AddYsPacketLog(ysp YsPackets) error {
	_, err := localdb.Insert(&ysp)
	return err
}

// UpdateYspacketLog ...
func (yuf *YsUploadFile) UpdateYspacketLog(fid string, sortid int, err error) {
	pf := new(YsPackets)
	if err != nil {
		pf.Status = 2
		pf.Msg = err.Error()
	} else {
		pf.Status = 0
	}
	localdb.Where("yid=? and sortid=?", fid, sortid).Cols("status", "msg").Update(pf)
}
