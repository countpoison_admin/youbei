package routers

import (
	"gitee.com/countpoison/youbei/controllers"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/plugins/cors"
)

func init() {
	beego.BConfig.MaxMemory = 1 << 26

	beego.InsertFilter("*", beego.BeforeRouter, cors.Allow(&cors.Options{
		AllowAllOrigins:  true,
		AllowMethods:     []string{"GET", "POST", "PUT", "DELETE"},
		AllowHeaders:     []string{"Origin", "Authorization", "Token", "Access-Control-Allow-Origin", "Access-Control-Allow-Headers", "Content-Type"},
		ExposeHeaders:    []string{"Content-Length", "Access-Control-Allow-Origin", "Access-Control-Allow-Headers", "Content-Type"},
		AllowCredentials: true,
	}))

	beego.Router("/api/task/?:id", &controllers.MainController{}, "get:GetTask;post:Add;delete:Del;put:Update")
	beego.Router("/api/tasks", &controllers.MainController{}, "get:Tasklist")
	beego.Router("/api/logs/?:id", &controllers.MainController{}, "get:Loglist")
	beego.Router("/api/log/:id", &controllers.MainController{}, "get:ShowLog")
	beego.Router("/api/rlogs/?:id", &controllers.MainController{}, "get:Rloglist")
	beego.Router("/api/rlog/:id", &controllers.MainController{}, "get:ShowrLog")
	beego.Router("/api/uploadlogs/?:id", &controllers.MainController{}, "get:Uploadlogs")
	beego.Router("/api/uploadlog/:id", &controllers.MainController{}, "get:GetYserverLog")
	beego.Router("/api/login/:name/:password", &controllers.LoginController{}, "get:Userlogin")
	beego.Router("/api/ftps", &controllers.MainController{}, "get:Ftplist")
	beego.Router("/api/ftp/?:id", &controllers.MainController{}, "post:Ftpadd;delete:Ftpdelete;get:Ftpfind;put:Ftpupdate")
	beego.Router("/upload/packet/:id/:sort", &controllers.LoginController{}, "post:Uploadpacket")
	beego.Router("/upload/file/:id", &controllers.LoginController{}, "post:UploadFile")
	beego.Router("/service/upload/?:id", &controllers.MainController{}, "put:EnableServer;delete:DisableServer;get:Yserverlist")
	beego.Router("/api/jobtest", &controllers.MainController{}, "post:JobTest")
	beego.Router("/api/runjob/:id", &controllers.MainController{}, "put:RunJob")
	beego.Router("/api/runsshjob/:id", &controllers.MainController{}, "put:RunSshJob")
	beego.Router("/api/dirlist", &controllers.MainController{}, "get:DirList")
	beego.Router("/api/hosts", &controllers.MainController{}, "get:HostsGet")
	beego.Router("/api/host/?:id", &controllers.MainController{}, "get:HostGet;put:HostUpdate;delete:HostDelete;post:HostAdd")
	beego.Router("/api/users", &controllers.MainController{}, "get:UserList")
	beego.Router("/api/user/?:id", &controllers.MainController{}, "get:GetUser;post:AddUser;put:EditUser;delete:DeleteUser")
	beego.Router("/api/pchange/:id", &controllers.MainController{}, "put:Userchangepwd")
	//beego.Router("/api/systeminfo", &controllers.MainController{}, "get:SystemInfo")
	beego.Router("/api/mailserver", &controllers.MainController{}, "post:MailTest;put:MailServerUpdate;get:GetMail")

	beego.Router("/api/sysinfo", &controllers.MainController{}, "get:Sysinfo")
	beego.Router("/api/connecthost/:id", &controllers.MainController{}, "put:ConnectHost")
	beego.Router("/api/sshtask/?:id", &controllers.MainController{}, "post:AddSshtask;put:UpdateSshtask;delete:DeleteSshtask;get:GetSshtask")
	beego.Router("/api/sshtasks", &controllers.MainController{}, "get:SshtaskList")
	beego.Router("/api/dashboardinfo", &controllers.MainController{}, "get:DashBoardInfo")

	beego.SetStaticPath("/", "static")

}
