package db

import (
	"time"

	//go-mssqldb ...
	_ "github.com/denisenkom/go-mssqldb"
	"github.com/xormplus/xorm"
)

//MssqlConnectTest ...
func MssqlConnectTest(Host, Dbname, User, Password string) error {
	db, err := xorm.NewEngine("mssql", "server="+Host+";user id="+User+";password="+Password+";database="+Dbname)
	defer db.Close()
	if err != nil {
		return err
	}
	err = db.Ping()
	return err
}

//MssqlSQLDump ...
func MssqlSQLDump(host, dbname, user, password, backupfilePath, zippwd string) (string, error) {
	db, err := xorm.NewEngine("mssql", "server="+host+";user id="+user+";password="+password+";database="+dbname)
	defer db.Close()
	if err != nil {
		return "", err
	}
	err = db.Ping()
	if err != nil {
		return "", err
	}

	nowtime := time.Now().Format("2006-01-02_15-04-05")
	dist := host + "_mssql_" + dbname + "_" + nowtime + ".sql"
	distzp := host + "_mssql_" + dbname + "_" + nowtime + ".zip"
	err = db.DumpAllToFile(backupfilePath + "/" + dist)
	if err != nil {
		return "", err
	}
	err = db.Ping()
	if err != nil {
		return "", err
	}
	return SQLDumpZip(backupfilePath+"/"+dist, backupfilePath+"/"+distzp, zippwd)

}
