package jobs

import (
	"os"
	"time"

	md "gitee.com/countpoison/youbei/models"
)

//ExpireDelete ...
func ExpireDelete(id string, expireTime int64) error {
	logs := []md.Log{}
	err := md.Localdb().Where("deleted=0 and created<? and tid=?", expireTime, id).Find(&logs)
	if err != nil {
		return err
	}
	for _, v := range logs {
		f, err := os.Stat(v.Localfilepath)
		if err == nil {
			if !f.IsDir() {
				log := new(md.Log)
				log.Deleted = time.Now().Unix()
				_, err := md.Localdb().ID(v.ID).Cols("deleted").Update(log)
				if err != nil {
					return err
				}
				os.RemoveAll(v.Localfilepath)
			}
		}
	}
	return nil
}
