package rs

import (
	"errors"
	"os"
	"strconv"

	"github.com/segmentio/ksuid"

	"github.com/astaxie/beego/httplib"
)

//resAPIPacketup ...
type resAPIPacketup struct {
	Success int         `json:"success"`
	Msg     string      `json:"msg"`
	Result  interface{} `json:"result"`
}

//UploadByte ...
func UploadByte(url string, b []byte) error {
	res := resAPIPacketup{}
	err := httplib.Post(url).Body(b).ToJSON(&res)
	return err

}

//uploadfile ...
func (c *Filepacket) uploadfile() error {
	res := resAPIPacketup{}
	req := httplib.Post(c.FileUploadURL)
	req.Param("username", c.UploadUser)
	req.Param("password", c.UploadPassword)
	req.Param("filename", c.FileName)
	req.Param("savedir", c.DstDir)
	req.Param("size", strconv.FormatInt(c.Size, 10))
	req.Param("packetnum", strconv.FormatInt(c.PacketNum, 10))
	err := req.ToJSON(&res)
	if err != nil {
		return err
	}
	if res.Success != 200 {
		return errors.New(res.Msg)
	}
	return nil
}

//ReadBigFile ...
func (c *RS) ReadBigFile(relink int) (*Filepacket, error) {
	fp, err := c.readMyFile()
	if err != nil {
		return nil, err
	}
	err = fp.uploadfile()
	if err != nil {
		return nil, err
	}
	return fp, nil
}

//Filepacket ...
type Filepacket struct {
	UploadFileServerID string

	FileUploadURL string
	FileName      string
	SrcDir        string
	SrcFilePath   string
	Size          int64

	DstDir      string
	DstFilePath string

	UploadUser     string
	UploadPassword string

	PacketUploadURL string
	Packets         map[int]Packet
	PacketSize      int64
	PacketNum       int64
}

//Packet ...
type Packet struct {
	SortID     int
	Packetpath string
	Offset     int64
	Error      error
}

//readMyFile ...
func (c *RS) readMyFile() (*Filepacket, error) {
	fp := new(Filepacket)
	fp.FileName = c.FileName
	fp.SrcFilePath = c.SrcFilePath
	fp.SrcDir = c.SrcDir

	fi, err := os.Stat(fp.SrcFilePath)
	if err != nil {
		return nil, err
	}
	fp.Size = fi.Size()
	fp.PacketSize = int64(20 * 1024 * 1024)
	fp.PacketNum = fp.Size / fp.PacketSize
	yu := fp.Size % fp.PacketSize
	if yu > 0 {
		fp.PacketNum++
	}
	fps := map[int]Packet{}
	for i := int64(0); i < fp.PacketNum; i++ {
		packet := Packet{}
		packet.SortID = int(i)
		packet.Offset = i * fp.PacketSize
		fps[packet.SortID] = packet
	}
	fp.Packets = fps
	fp.UploadFileServerID = ksuid.New().String()
	fp.FileUploadURL = "http://" + c.Host + ":" + strconv.Itoa(c.Port) + "/upload/file/" + fp.UploadFileServerID
	fp.PacketUploadURL = "http://" + c.Host + ":" + strconv.Itoa(c.Port) + "/upload/packet/" + fp.UploadFileServerID + "/"
	fp.DstDir = c.DstDir
	fp.DstFilePath = c.DstFilePath
	fp.UploadUser = c.UploadUser
	fp.UploadPassword = c.UploadPassword
	return fp, nil
}

//CreatePacket ...
func (c *Filepacket) CreatePacket(file *os.File, v Packet) error {
	buf := make([]byte, c.PacketSize)
	nr, _ := file.ReadAt(buf[:], v.Offset)
	var err error
	if nr == 0 {
		err = nil
	} else if nr > 0 {
		err = UploadByte(c.PacketUploadURL+strconv.Itoa(v.SortID), buf[0:nr])
	} else {
		err = errors.New("nr < 0")
	}
	return err
}
